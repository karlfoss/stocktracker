# README #

This is a test project that uses [this api](https://financialmodelingprep.com/developer/docs/#Ticker-Search) in order
to test getting stock information and outputting it to a .csv file

* Author: Karl Foss
* Git repo: https://bitbucket.org/karlfoss/stocktracker/src/master/

### Setup ###
 
* Run "git clone https://bitbucket.org/karlfoss/stocktracker.git" to pull in the repo
* Run "npm i" in the terminal to install all necessary packages
* Run "npm start" to run the program, which will update all stocks specified in the stock object in index.js
* Run "npm start true" to turn on debugging, which will do the same as "npm start" and then outputs more logs
* Run "npm test" to run the unit tests