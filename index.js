// Sources:
// https://financialmodelingprep.com/developer/docs/#Ticker-Search
//   
// All of the stocks to fetch the information for (uses stock symbols)
var stocks = [
  {
    Ticker: "MSFT",
    Quantity: 3.074,
    CurrentPrice: 0,
    High: 0,
    Low: 0,
    CurrentValue: 0
  },
  {
    Ticker: "AAPL",
    Quantity: 2.225,
    CurrentPrice: 0,
    High: 0,
    Low: 0,
    CurrentValue: 0
  },
  {
    Ticker: "MCD",
    Quantity: 10.400,
    CurrentPrice: 0,
    High: 0,
    Low: 0,
    CurrentValue: 0
  },
  {
    Ticker: "CAT",
    Quantity: 56.551,
    CurrentPrice: 0,
    High: 0,
    Low: 0,
    CurrentValue: 0
  },
  {
    Ticker: "WHR",
    Quantity: 1.2222,
    CurrentPrice: 0,
    High: 0,
    Low: 0,
    CurrentValue: 0
  },
];
var debug;
// set debug to true or false;
if (process.argv.slice(2) == 'true')
{
  debug = true;
}
else {
  debug = false;
}
console.log("Debug is set to " + debug);
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const StockAPIManager = require("./stock-manager-api.js");
const stockAPIManager = new StockAPIManager(debug);
const csvWriter = createCsvWriter({
  path: 'output.csv',
  header: [
    {id: 'Ticker', title: 'Ticker'},
    {id: 'Quantity', title: 'Quantity'},
    {id: 'CurrentPrice', title: 'CurrentPrice'},
    {id: 'High', title: 'High'},
    {id: 'Low', title: 'Low'},
    {id: 'CurrentValue', title: 'CurrentValue'},
  ]
});

// Below four steps are synchronously executed, but could be enhanced in the future
// Getting the stock history and stock price information could both be done at the same time
getStockHistory(stocks);

// Get all of the price information on each stock between 2019-01-01 and today
function getStockHistory(stocks) {
  stockAPIManager.getStockPriceHistory(stocks, (data) => {
    getStockPrice(stocks);
  });
}

// Get the current stock price for each stock
function getStockPrice(stocks) {
    stockAPIManager.getCurrentStockPrices(stocks, (data) => {
    getStockValue(stocks);
  });
}

// Update the current stock value with quantity * current stock price
function getStockValue(stocks) {
  stockAPIManager.setCurrentStockValue(stocks, (data) => {
    console.log(data);
    writeStocksToFile(stocks);
  });
}

// Output stock information to a file
function writeStocksToFile() {
  csvWriter
  .writeRecords(stocks)
  .then(()=> console.log('The CSV file was written successfully'));
}