const StockAPIManager = require("../stock-manager-api.js");
const stockAPIManager = new StockAPIManager(false);
const assert = require('assert').strict;

describe("Check Stock History", () => {
  it('Verify Microsoft Stock Price History', async () => {
    let outputStocks = [];
    let inputStocks = [
      {
        Ticker: "MSFT",
        Quantity: 1.0,
        CurrentPrice: 0,
        High: 0,
        Low: 0,
        CurrentValue: 0
      }];
    // Microsoft High and Lows between 2019-01-01 and 2020-03-04 - test will break if these are beaten
    const MSFT_HIGH = 190.65;
    const MSFT_LOW = 99.55;
  
    await stockAPIManager.getStockPriceHistory(inputStocks, (data) => {
      outputStocks = data;
    });
    //console.log(outputStocks);
    assert.equal(outputStocks[0].Low, MSFT_LOW);
    assert.equal(outputStocks[0].High, MSFT_HIGH);
  });
});

