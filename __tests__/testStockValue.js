const StockAPIManager = require("../stock-manager-api.js");
const stockAPIManager = new StockAPIManager(false);
const assert = require('assert').strict;

describe("Check Stock Value Calculation", () => {
  it('Verify Microsoft Stock Value Calculation', async () => {
    let outputStocks = [];
    let inputStocks = [
      {
        Ticker: "MSFT",
        Quantity: 1.17,
        CurrentPrice: 1975.01,
        High: 0,
        Low: 0,
        CurrentValue: 0
      }];
    const EXPECTED_VALUE = "$" + inputStocks[0].Quantity * inputStocks[0].CurrentPrice;
  
    await stockAPIManager.setCurrentStockValue(inputStocks, (data) => {
      outputStocks = data;
    });
    console.log(outputStocks);
    assert.equal(outputStocks[0].CurrentValue, EXPECTED_VALUE);
  });
});

