const StockAPIManager = require("../stock-manager-api.js");
const stockAPIManager = new StockAPIManager(false);
const assert = require('assert').strict;

describe("Check Stock Price Get", () => {
  it('Verify Microsoft Stock Price', async () => {
    let outputStocks = [];
    let inputStocks = [
      {
        Ticker: "MSFT",
        Quantity: 1.0,
        CurrentPrice: 0,
        High: 0,
        Low: 0,
        CurrentValue: 0
      }];
  
    await stockAPIManager.getCurrentStockPrices(inputStocks, (data) => {
      outputStocks = data;
    });
    //console.log(outputStocks);
    assert.notEqual(outputStocks[0].CurrentPrice, 0);
  }),
  it('Verify Multiple Stock Prices', async () => {
    let outputStocks = [];
    let inputStocks = [
      {
        Ticker: "MSFT",
        Quantity: 1.0,
        CurrentPrice: 0,
        High: 0,
        Low: 0,
        CurrentValue: 0
      },
      {
        Ticker: 'AAPL',
        Quantity: 1.0,
        CurrentPrice: 0,
        High: 0,
        Low: 0,
        CurrentValue: 0
      }];
  
    await stockAPIManager.getCurrentStockPrices(inputStocks, (data) => {
      outputStocks = data;
    });
    //console.log(outputStocks);
    assert.notEqual(outputStocks[0].CurrentPrice, 0);
    assert.notEqual(outputStocks[1].CurrentPrice, 0);
  });
});

