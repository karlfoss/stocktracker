//TODO: to enhance this api, use parallel calls for the get requests (https://itnext.io/node-js-handling-asynchronous-operations-in-parallel-69679dfae3fc)
const axios = require("axios")
var debug;

class StockAPIManager
{
  // Set to true to print additional console logs;
  constructor (isDebug) {
    debug = isDebug;
  }

  // Synchronously gets all of the price history of each stock
  // Then does a callback with the updated stock values
  async getCurrentStockPrices(stocks, callback)
  {
    stocks = await getAllCurrentStockPrices(stocks);
    return callback(stocks);
  }

  // Synchronously gets all of the price history of each stock
  // Then does a callback with the updated stock information
  async getStockPriceHistory(stocks, callback) {
    
    stocks = await getAllStockPriceHistory(stocks);
    return callback(stocks);

  }

  // Synchronously recalculates the price based on the quantity and the current price
  async setCurrentStockValue(stocks, callback) {
    if (debug) { console.log("start update stock value")};
    try {
      await setValueOfStocks(stocks);
    } catch (error) {
      console.log(error);
    }
    
    if (debug) { console.log('end update stock value')};
    return callback(stocks);
  }
}

// Synchronously gets all of the current prices of each stock
async function getAllCurrentStockPrices(stocks) {
  if (debug) { console.log("start get current stock prices")};
  for (let i = 0; i < stocks.length; i++)
  {
    try {
      const response = await axios.get('https://financialmodelingprep.com/api/v3/stock/real-time-price/' + stocks[i].Ticker);
      const data = response.data;
      stocks[i].CurrentPrice = data["price"];
      if (debug) {console.log(data)};
    } catch (error) {
      console.log(error);
    }
  }
  if (debug) { console.log("end get current stock prices")};
  return stocks;
}

// Synchronously gets all of the price history of each stock
async function getAllStockPriceHistory(stocks) {

  let currentDate = getCurrentDate();

  if (debug) { console.log("get stock price history")};
  for (let i = 0; i < stocks.length; i++)
  {
    try {
      const response = await axios.get('https://financialmodelingprep.com/api/v3/historical-price-full/' + stocks[i].Ticker + '?from=2019-01-01&to=' + currentDate);
      const data = response.data;
      const stockHistory = data["historical"];
      var allStockOpens = stockHistory.map(stockHistory => stockHistory["open"]);
      // Get the highest value between 2019 and the current date at the market open for the stock
      stocks[i].High = Math.max(...allStockOpens);
      stocks[i].Low = Math.min(...allStockOpens);
      if (debug) { console.log ("Stock: " + stocks[i].Ticker + " Low: " + stocks[i].Low + " High: " + stocks[i].High) };
    } catch (error) {
      console.log(error);
    }
  }

  if (debug) { console.log("end stock price histories")};
  return stocks;
}

// Calculate the value of the stock based on current price * quantity
async function setValueOfStocks(stocks) {
  for (let i = 0; i < stocks.length; i++)
  {
    stocks[i].CurrentValue = "$" + (stocks[i].Quantity * stocks[i].CurrentPrice);
  }
  return stocks;
}

// Returns the current date in the format YYYY-MM-DD
function getCurrentDate() {
  let currentDate = new Date();
  // current day of the month
  let date = ("0" + currentDate.getDate()).slice(-2);
  // current month
  let month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
  // current year
  let year = currentDate.getFullYear();
  return year + "-" + month + "-" + date;
};

// Unused function for testing if the asynchronous functions were working as expected.
function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

module.exports = StockAPIManager;